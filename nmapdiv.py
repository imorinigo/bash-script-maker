import argparse
import subprocess
import os

def conocer_ips_totales(archivo):
	ips_totales = 0
	file = open(archivo, "r")
	for linea in file.readlines():
		ips_totales += 1
	file.close()
	return ips_totales

def main():

	print("\n=========== Bash Script Maker ===========\n")

	parser = argparse.ArgumentParser(description='Crea scripts bash para ejecutar comandos a partir de un archivo con las IPs')

	parser.add_argument('-a', '--archivo', dest="archivo", help='Ruta del archivo que contiene las IPs')
	parser.add_argument('-n', '--nmap', dest="nmap", help='Selecciona el tipo de namp a ejecutar')
	parser.add_argument('-sh', '--shodan', dest="shodan", help='Utiliza el script de nmap de shodan para relaizar consultas sobre las ips del archivo, requiere key [No realiza un nuevo scan]')
	parser.add_argument('-s', '--simultaneo', dest="simultaneo", default=1, type=int, help='La cantidad de nmaps simultáneos que se desean correr. (Default: 1, no hay simultaneidad)')
	parser.add_argument('-c', '--cantidad', dest="cantidad", type=int, help='La cantidad de nmaps totales por script bash. (Default: La misma cantidad que el archivo con las IPs)')
	args = parser.parse_args()
	
	file = open(args.archivo, "r")
	if (args.nmap == 'tcp-t100'):
		lista = []
		for linea in file.readlines():
			ip = linea.replace('\n','')
			comando = "nmap -Pn -sSV -vvv --top-ports 100 "+ip+" -oA "+ip+"-nmap_tcp_top_100 &\n"
			#Si se ingresó 1 en simultaneidad o nada, evita la simultaneidad haciendo que el comando no se ejecute en segundo plano (quitando el caracter "&")
			if( args.simultaneo == 1 ):
				comando = comando.replace(' &','')
			lista.append(comando)
	elif (args.nmap == 'udp-t20'):
		lista = []
		for linea in file.readlines():
			ip = linea.replace('\n','')
			comando = "nmap -Pn -sUV -vvv --top-ports 20 "+ip+" -oA "+ip+"-nmap_udp_top_20 &\n"
			if( args.simultaneo == 1 ):
				comando = comando.replace(' &','')
			lista.append(comando)
	elif (args.nmap == 'tcp-full'):
		lista = []
		for linea in file.readlines():
			ip = linea.replace('\n','')
			comando = "nmap -Pn -sSV -vvv -p- "+ip+" -oA "+ip+"-nmap_tcp_full &\n"
			if( args.simultaneo == 1 ):
				comando = comando.replace(' &','')
			lista.append(comando)
	elif (args.shodan != ''):
		lista = []
		for linea in file.readlines():
			ip = linea.replace('\n','')
			comando = "nmap -sn -Pn -n --script=shodan-api -script-args 'shodan-api.apikey="+args.shodan+"' "+ip+" -oA "+ip+"-shodan &\n"
			if( args.simultaneo == 1 ):
				comando = comando.replace(' &','')
			lista.append(comando)
	file.close()
	
	ips_totales = conocer_ips_totales(args.archivo)
	
	#Define el valor por defecto si no se ingresó un valor para cantidad de comandos por script
	if( args.cantidad is None):
		args.cantidad = conocer_ips_totales(args.archivo)
		cant_scripts = 1
	else:
		cant_scripts =int( ips_totales / args.cantidad )	

	print("IPs totales: " + str(ips_totales)+ "\n")
	print("Cantidad de comandos por script: " + str(args.cantidad) + "\t Cantidad de scripts en ejecución simultánea: " + str(args.simultaneo))
	if(args.simultaneo == 1):
		print("\n\t[-]La simultaneidad en 1 significa que no habrá ejecuciones en segundo plano")
	print("\n[+]Se crearán en total " + str(cant_scripts) + " scrips...\n")

	#Contador se utiliza para no perder la posición en la lista, cuando se termina el segundo for, de ésa manera toda la lista es escrita en distintos archivos.
	contador = 0
	nombres_scripts = []
	for y in range(0, cant_scripts):
		#ContadorWait se utiliza de la misma manera que contador, pero su función sirve para definir en que línea escribir wait
		contadorWait = 0
		#Se abren los archivos bash, el nombre es iterativo
		script = open("sc"+str(y+1)+".sh","w")
		nombres_scripts.append("sc"+str(y+1)+".sh")
		
		for x in range(0+contador, args.cantidad+contador):
			if(contadorWait % args.simultaneo == 0 and contadorWait != 0 and args.simultaneo != 1):
				script.write("wait\n")
			script.write(lista[x])
			contador+=1
			contadorWait+=1
		
		if (( ips_totales % cant_scripts )!= 0 and y == args.cantidad-1):
			script.write(lista[contador])
	print("[+]Script(s) creado(s): \n")
	for element in nombres_scripts:
		print("\t"+element)
	print("\nEjecución exitosa")

if __name__ == '__main__':
	main()