import subprocess, time, argparse

parser = argparse.ArgumentParser(description='Ejecuta los comandos guardados en un archivo')

parser.add_argument('-a', '--archivo', dest="archivo", help='Ruta del archivo que contiene los comandos')
parser.add_argument('-s', '--simultaneo', dest="simultaneo", default=1, type=int, help='La cantidad de comandos simultáneos que se desean correr. (Default: 1, se ejecuta de un comando a la vez)')
parser.add_argument('-t', '--tiempo', dest="tiempo", type=int, default=None, help='El tiempo de ejecucion que deben permanecer corriendo los comandos')

args = parser.parse_args()

file_comandos = open(args.archivo, "r")

if(args.tiempo != None):
	tiempo_corte = time.time() + args.tiempo

print("Se ejecutarán los codigos del archivo: " + args.archivo + "\n")

if(args.simultaneo == 1 and args.tiempo != None):
	for line in file_comandos.readlines():
		if (time.time() < tiempo_corte):	
			line = line.replace('\n','')
			print("[+]Start: "+line)
			linea = line.split()
			tiempo_inicio_comando = time.time()
			try:
				x = subprocess.run(linea,stdout=subprocess.PIPE, stderr=subprocess.PIPE, timeout=(tiempo_corte - tiempo_inicio_comando))
				print("...Completed")
			except subprocess.TimeoutExpired:
				z = "".join(line)
				print("\nTimeout\n[-]Killed: "+ z)
		else:
			print("[-]Not Started: "+ line.replace('\n',''))
elif(args.simultaneo == 1 and args.tiempo == None):
	for line in file_comandos.readlines():	
		line = line.replace('\n','')
		print("[+]Start: "+line)
		linea = line.split()
		try:
			x = subprocess.run(linea,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			print("...Completed")
		except:
			z = "".join(line)
			print("\nProceso interrumpido\n[-]Killed: "+ z)
			break
		print("[-]Not Started: "+ line.replace('\n',''))
elif(args.simultaneo != 1 and args.tiempo != None):

	procs = []
			
	for line in file_comandos.readlines():
		line = line.replace('\n','')
		print("[+]Start: "+line)
		linea = line.split()
		tiempo_inicio_comando = time.time()
		x =  subprocess.Popen(linea,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		procs.append(x)
	
	time.sleep(int(tiempo_corte - tiempo_inicio_comando))
	print("\n")
		
	for p in procs:	
		comando = ' '.join(p.args)
		print("\Timeout\n[-]Killed: "+ comando)
		p.kill()
		
file_comandos.close()

print("\n")


"""
		
#%H:%M:%S	
		
"""

	

	

	
"""
import subprocess

try:
	proc = subprocess.run(['bash', 'sc1.sh'], timeout=15)
except:
	print('error')

import subprocess, os, time
try:
	#procs = []
	#or i in range
	p = subprocess.run(['./sc1.sh'], timeout=15, shell=True)
except:
	#time.sleep(10)
	print(p.pid)
	p.kill()
	

    for i in range(0,number):
        x = subprocess.Popen(start,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)#Launch another script as a subprocess
        procs.append(x)
    # Do stuff
    ...
    # Now kill all the subprocesses
    for p in procs:
        p.kill()

"""
